#!/usr/bin/python

from typing import List
from collections import defaultdict
from enum import Enum
import math
import json

from os import environ
environ['PYGAME_HIDE_SUPPORT_PROMPT'] = '1'
import pygame
from pygame.locals import *

from settings import *

class Float:
    """ static functions for comparing floats """
    @staticmethod
    def eq(a, b):
        return math.isclose(b, a, abs_tol=1e-06)
    @staticmethod
    def lte(a, b):
        return a <= b or math.isclose(b, a, abs_tol=1e-06)
    @staticmethod
    def lt(a, b):
        return a < b and not math.isclose(b, a, abs_tol=1e-06)
    @staticmethod
    def gt(a, b):
        return a > b and not math.isclose(b, a, abs_tol=1e-06)

class Tile_map:
    def __init__(self, filename):
        self.tiles = []
        self.load_map(filename)

    def load_map(self, filename):
        image = pygame.image.load(filename).convert_alpha()
        w, h = image.get_size()
        for j in range(h //TILESIZE):
            for i in range(w // TILESIZE):
                rect = (i * TILESIZE, j * TILESIZE, TILESIZE, TILESIZE)
                self.tiles.append(image.subsurface(rect))

class Cell:

    def __init__(self, i, j, tile=None, obstacle=True):
        self.i = i
        self.j = j
        self.tile = tile
        self.lighted = False
        self.obstacle = obstacle
        self.edges = {}

    def get_xy(self):
        return (
            (self.i * TILESIZE, self.i * TILESIZE + TILESIZE),
            (self.j * TILESIZE, self.j * TILESIZE + TILESIZE)
        )

    def clicked(self, pos):
        """ for debugging: print the cell's coordinates """
        x, y = self.get_xy()
        if x[0] < pos[0] < x[1] and y[0] < pos[1] < y[1]:
            print(x, y)

    @staticmethod
    def is_point_in_triangle(p, a, b, c):
        """ https://stackoverflow.com/a/9755252/5239250 """

        ap_x = p.x - a.x
        ap_y = p.y - a.y

        p_ab = (b.x - a.x) * ap_y - (b.y - a.y) * ap_x >= 0

        p_ac = (c.x - a.x) * ap_y - (c.y - a.y) * ap_x >= 0

        if p_ac == p_ab:
            return False

        p_bc = (c.x - b.x) * (p.y - b.y) - (c.y - b.y) * (p.x - b.x) >= 0

        if p_bc != p_ab:
            return False

        return True

    @staticmethod
    def is_basis_in_edge(x, y, basis):
        """
        if the basis is horizontal, check if it's on the lower or higher cell y, then if the basis x is between the cell's xs.
        use the left and right edges, if it's vertical
        """
        if basis[0][1] == basis[1][1]:
            if basis[0][1] in y:
                if basis[0][0] >= x[0] and basis[1][0] <= x[1]:
                    return True
        elif basis[0][0] == basis[1][0]:
            if basis[0][0] in x:
                if basis[0][1] >= y[0] and basis[1][1] <= y[1]:
                    return True

        return False

    def update_lighted(self, light):
        """
        a cell is lighted if
        - one of its corner points (x[0], y[0], x[1], y[1]) is inside of one of the light triangles.
        - one of the triangle bases is on one of its edges

        """

        x, y = self.get_xy()

        points = (Point(x[0], y[0]), Point(x[0], y[1]), Point(x[1], y[0]), Point(x[1], y[1]))
        self.lighted = False

        for point in points:
            for triangle in light.get_next_triangle_points():
                if Cell.is_point_in_triangle(point, *triangle):
                    self.lighted = True
                    return

        for triangle in light.get_next_triangle_basis():
            if Cell.is_basis_in_edge(x, y, triangle):
                self.lighted = True
                return

    @staticmethod
    def segments_intersect(s0, s1):
        """
        simple solution when s0 is always horizontal or vertical
        see https://stackoverflow.com/questions/3838329/how-can-i-check-if-two-segments-intersect for a general solution
        """
        s0 = sorted(s0, key=lambda xy: (xy[1], xy[0]))
        s1 = sorted(s1, key=lambda xy: (xy[1], xy[0]))
        x0, y0 = s0[0]
        x1, y1 = s0[1]
        x2, y2 = s1[0]
        x3, y3 = s1[1]

        if (max(x0, x1) < min(x2, x3) or max(y0, y1) < min(y2, y3)):
            return False

        # s2 is vertical
        if x2 == x3:
            if x0 == x1:
                return False
            else:
                return x2 >= x0 and x2 <= x1

        a = (y2 - y3) / (x2 - x3)
        b = y3 - a * x3

        if x0 == x1:
            y = b + a * x0
            return y >= y0 and y <= y1
        if y0 == y1:
            x = (y0 - b) / a
            return x >= x0 and x <= x1

    def draw(self, screen, tile_map):
        if not self.lighted or WIREFRAME:
            return
        # pygame.draw.rect(screen, YELLOW, (self.i * TILESIZE, self.j * TILESIZE, TILESIZE, TILESIZE))
        screen.blit(tile_map.tiles[self.tile], (self.i * TILESIZE, self.j * TILESIZE, TILESIZE, TILESIZE))

Direction = Enum('Direction', 'H V')

class Edge:
    def __init__(self, i, j, side, cell=None, n=1):
        self.i = i
        self.j = j
        self.cell = cell
        self.side = side
        self.x = i * TILESIZE
        if side == 'E':
            self.x += TILESIZE
        self.y = j * TILESIZE
        if side == 'S':
            self.y += TILESIZE
        if side == 'E' or side == 'W':
            self.direction = Direction.V
        if side == 'N' or side == 'S':
            self.direction = Direction.H
        self.n = n
        self.length = n * TILESIZE

    def __str__(self):
        return f'({self.i}, {self.j}, {self.n})'

    def get_end(self):
        if self.direction == Direction.H:
            return self.x + self.length, self.y
        else:
            return self.x, self.y + self.length

    def grow(self, size=1):
        self.n += size
        self.length += size * TILESIZE

    def draw(self, screen):
        if not WIREFRAME:
            return
        if self.direction == Direction.H:
            pygame.draw.line(screen, GREEN, (self.x, self.y), (self.x + self.length, self.y), 2)
            pygame.draw.circle(screen, RED, (self.x + self.length, self.y), 4)
        else:
            pygame.draw.line(screen, GREEN, (self.x, self.y), (self.x, self.y + self.length), 2)
            pygame.draw.circle(screen, RED, (self.x, self.y + self.length), 4)
        pygame.draw.circle(screen, RED, (self.x, self.y), 4)

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __hash__(self):
        # return hash(f'{self.x},{self.y}')
        return self.x * 10000 + self.y

    # def __lt__(self, other):
    #   return (self.y, self.x) < (other.y, other.x)

    def __eq__(self, other):
        return (self.y, self.x) == (other.y, other.x)

    def __str__(self):
        return f'({self.x}, {self.y})'

class Corner(Point):
    def __init__(self, x, y, cell = None):
        super().__init__(x, y)
        self.cell = cell

class LightPolygon:
    def __init__(self, angle, x, y):
        self.angle = angle
        self.x = x
        self.y = y

class Light:
    def __init__(self, i, j, radius):
        self.i = i
        self.j = j
        self.tile = None
        self.x = None
        self.y = None
        self.update_x_y()
        self.next_movement = (0, 0)
        self.radius = radius
        self.triangle_points = []
        self.corners = None # cache the values when called from world
        self.edges = None
        self.cells = None
        self.key_press_delay = 0

    def update_x_y(self):
        self.x = self.i * TILESIZE + TILESIZE // 2
        self.y = self.j * TILESIZE + TILESIZE // 2

    def update(self, cells, dt):
        """
        :return bool True if something has changed
        """
        if self.key_press_delay > 0 and self.key_press_delay < KEY_REPEAT:
            self.key_press_delay += dt
            return False

        keys = pygame.key.get_pressed()
        if keys[pygame.K_UP]:
            self.next_movement = (0, -1)
        elif keys[pygame.K_DOWN]:
            self.next_movement = (0, 1)
        elif keys[pygame.K_LEFT]:
            self.next_movement = (-1, 0)
        elif keys[pygame.K_RIGHT]:
            self.next_movement = (1, 0)

        if self.next_movement != (0, 0):
            self.key_press_delay = dt
            i = self.i + self.next_movement[0]
            j = self.j + self.next_movement[1]
            if (i, j) in cells and cells[(i, j)].obstacle:
                self.next_movement = (0, 0)
                return False
            self.i = min(max(i, 0), GRIDWIDTH - 1)
            self.j = min(max(j, 0), GRIDHEIGHT - 1)
            self.update_x_y()
            self.next_movement = (0, 0)

            self.calculate_triangle_points()
            return True
        else:
            self.key_press_delay = 0
            return False


    def events(self, event):
        pass
        # if event.type == KEYDOWN:
        #     if event.key == K_UP:
        #         self.next_movement = (0, -1)
        #     elif event.key == K_DOWN:
        #         self.next_movement = (0, 1)
        #     if event.key == K_LEFT:
        #         self.next_movement = (-1, 0)
        #     elif event.key == K_RIGHT:
        #         self.next_movement = (1, 0)

    def get_next_triangle_basis(self):
        prev_p = self.triangle_points[0]
        for p in self.triangle_points[1:]:
            yield sorted((prev_p[1:], p[1:]), key=lambda xy: (xy[1], xy[0]))
            prev_p = p
        p = self.triangle_points[0]
        yield sorted((prev_p[1:], p[1:]), key=lambda xy: (xy[1], xy[0]))

    def get_next_triangle_points(self):
        prev_p = self.triangle_points[0]
        for p in self.triangle_points[1:]:
            yield Point(self.x, self.y), Point(*prev_p[1:]), Point(*p[1:])
            prev_p = p
        p = self.triangle_points[0]
        yield Point(self.x, self.y), Point(*prev_p[1:]), Point(*p[1:])

    def draw_cone(self, screen):
        for p0, p1 in self.get_next_triangle_basis():
            if WIREFRAME:
                pygame.draw.line(screen, ORANGE, (self.x, self.y), p0)
            else:
                pygame.draw.polygon(screen, LIGHTORANGE, ((self.x, self.y), p0, p1))

    def draw_tile(self, screen, tile_map):
        # pygame.draw.rect(screen, ORANGE, (self.i * TILESIZE, self.j * TILESIZE, TILESIZE, TILESIZE), 1 if WIREFRAME else 0)
        screen.blit(tile_map.tiles[self.tile], (self.i * TILESIZE, self.j * TILESIZE, TILESIZE, TILESIZE))

    def cache_corners_edges(self, corners, edges):
        if corners is None:
            if self.corners is not None:
                corners = self.corners
        else:
            self.corners = corners
        if edges is None:
            if self.edges is not None:
                edges = self.edges
        else:
            self.edges = edges

        return corners, edges

    def get_closest_intersection(self, angle, edges):
        """ given the light as origin and an angle to a corner,
            find the closest edge intersecting with the line """
        rdx = self.radius * math.cos(angle)
        rdy = self.radius * math.sin(angle)

        min_t1, min_x, min_y, min_edge = None, None, None, None
        # check the intersection with the edges
        for edge in edges:
            if edge.cell is not None and not edge.cell.obstacle:
                continue
            x0, y0 = edge.x, edge.y
            x1, y1 = edge.get_end()
            sdx = x1 - x0
            sdy = y1 - y0

            # normalised distance from line segment start to line segment end of intersect point
            # (vertical and horizontal must be handled separately)
            if rdy == 0 and sdy == 0:
                t1 = 1
            elif Float.eq(rdx, 0.0):
                t1 = (self.y - y0) / 1000
                if self.y < y0:
                    t1 *= -1
            else:
                t2 = (rdx * (y0 - self.y) + rdy * (self.x - x0)) / (sdx * rdy - sdy * rdx)
                # normalised distance from source along ray to ray length of intersect point
                t1 = (x0 + sdx * t2 - self.x) / rdx

                # If no intersect point exists along ray or along line segment
                # then intersect point is not valid
                # if t1 <= 0 or t2 < 0 or t2 > 1.0
                if Float.lte(t1, 0.0) or Float.lt(t2, 0.0) or Float.gt(t2, 1.0):
                    continue
            # reject this intersect point if farther than min
            if not min_t1 is None and t1 >= min_t1:
                continue

            min_t1 = t1
            min_x = self.x + rdx * t1
            min_y = self.y + rdy * t1
            min_edge = edge

        if min_x is None:
            return None, None, None, None

        min_ang = math.atan2(min_y - self.y, min_x - self.x)
        return int(round(min_x)), int(round(min_y)), min_ang, min_edge

    def calculate_triangle_points(self, corners=None, edges=None):
        """ calculate light triangles by checking where the light rays intersect with the obstacles """
        corners, edges = self.cache_corners_edges(corners, edges)
        if corners is None:
            return

        # TODO: make it a list of LightPolygon?
        self.triangle_points = []

        for point in corners:
            if point.cell is not None and not point.cell.obstacle:
                continue
            base_angle = math.atan2(point.y - self.y, point.x - self.x)
            x, y, _, _ = self.get_closest_intersection(base_angle, edges)
            if point.x != x or point.y != y:
                continue

            self.triangle_points.append((base_angle, point.x, point.y))

            x, y, _, edge = self.get_closest_intersection(base_angle - 0.0001, edges + World.border_edges)
            if edge is not None:
                x0, y0, x1, y1 = edge.x, edge.y, *edge.get_end()
                if not ((point.x == x0 and point.y == y0) or (point.x == x1 and point.y == y1)):
                    self.triangle_points.append((base_angle - 0.0001, x, y))

            x, y, _, edge = self.get_closest_intersection(base_angle + 0.0001, edges + World.border_edges)
            if edge is not None:
                x0, y0, x1, y1 = edge.x, edge.y, *edge.get_end()
                if not ((point.x == x0 and point.y == y0) or (point.x == x1 and point.y == y1)):
                    self.triangle_points.append((base_angle + 0.0001, x, y))
                    continue
        # sort by the angle
        self.triangle_points.sort(key=lambda i: i[0])

class Level_00:
    def __init__(self):
        self.config = Level.read(0)
        self.cells = Level.read_map(self.config['map'])
        self.goal = [coord for coord, c in self.cells.items() if c == 'O'][0]

    def get_cells(self):
        cells = {}
        for coord, c in self.cells.items():
            if c not in self.config['tiles']:
                continue
            c_config = self.config['tiles'][c]
            cell = Cell(*coord)
            cell.tile = c_config['tile']
            if 'obstacle' in c_config:
                cell.obstacle = c_config['obstacle']
            cells[coord] = cell
        return cells

    def get_light(self):
        x, y = [coord for coord, c in self.cells.items() if c == '*'][0]
        light = Light(x, y, 1000)
        light.tile = self.config['light']['*']['tile']
        return light

    def check_goal(self, cells, light):
        return self.goal[0] == light.i and self.goal[1] == light.j

class Level_01:
    def __init__(self):
        self.config = Level.read(1)
        self.cells = Level.read_map(self.config['map'])
        self.goal = [coord for coord, c in self.cells.items() if c == 'O'][0]

    def get_cells(self):
        cells = {}
        for coord, c in self.cells.items():
            if c not in self.config['tiles']:
                continue
            c_config = self.config['tiles'][c]
            cell = Cell(*coord)
            cell.tile = c_config['tile']
            if 'obstacle' in c_config:
                cell.obstacle = c_config['obstacle']
            cells[coord] = cell
        return cells

    def get_light(self):
        x, y = [coord for coord, c in self.cells.items() if c == '*'][0]
        light = Light(x, y, 1000)
        light.tile = self.config['light']['*']['tile']
        return light

    def check_goal(self, cells, light):
        return self.goal[0] == light.i and self.goal[1] == light.j

class Level:
    levels = [
        lambda: Level_00(),
        lambda: Level_01()
    ]

    def __init__(self, level):
        self.level = level

    @staticmethod
    def create(level):
        return Level.levels[level]()

    @staticmethod
    def read(level):
        with open(f'levels/level-{level:02d}.json') as f:
            return json.load(f)

    @staticmethod
    def read_map(filename):
        cells = {}

        with open(filename) as f:
            j = 0
            for line in f:
                i = 0
                for c in line.rstrip():
                    if c != ' ':
                        cells[(i, j)] = c
                    i += 1
                    if i >= GRIDWIDTH:
                        break
                j += 1
                if j >= GRIDHEIGHT:
                    break
        return cells

class World:
    border_edges = [
        Edge(0, 0, 'N', n=GRIDWIDTH),
        Edge(GRIDWIDTH - 1, 0, 'E', n=GRIDHEIGHT),
        Edge(0, GRIDHEIGHT - 1, 'S', n=GRIDWIDTH),
        Edge(0, 0, 'W', n=GRIDHEIGHT)
    ]
    border_corners = [
        Corner(0, 0),
        Corner(WIDTH, 0),
        Corner(WIDTH, HEIGHT),
        Corner(0, HEIGHT)
    ]
    def __init__(self):
        self.current_level = 0
        self.level = Level.create(self.current_level)
        self.cells = self.level.get_cells()
        self.edges = []
        # TODO: define the right radius!
        self.light = self.level.get_light()
        self.update_polygons()
        self.light.calculate_triangle_points(self.get_obstacles_corners(), self.edges + World.border_edges)
        for _, cell in self.cells.items():
            cell.update_lighted(self.light)
        self.editor_mode = False
        self.editor_save = False
        self.editor_type = False
        self.last_switched = None
        self.clicked = False
        self.tile_map = Tile_map('assets/tilemap.png')

    def update(self, dt):
        # TODO: move to an editor object
        if self.editor_mode:
            if self.clicked:
                # TODO: we might want to get the pos in events... it's more *exact"
                self.last_switched = tuple([xy // TILESIZE for xy in pygame.mouse.get_pos()])
                self.switch_cell(self.last_switched[0], self.last_switched[1])
            elif self.editor_type:
                if self.last_switched is not None:
                    cell = self.cells[self.last_switched[1]][self.last_switched[0]]
                    if cell.tile is not None:
                        cell.tile += 1
            elif self.editor_save:
                with open('levels/map_output.txt', 'w') as f:
                    f.write('\n'.join([''.join([' ' if c.tile is None else str(c.tile) for c in row]) for row in self.cells]))
                    print('write map_output.txt...')

            self.editor_save = False
            self.editor_type = False
        self.clicked = False
        if self.light.update(self.cells, dt):
            for _, cell in self.cells.items():
                cell.update_lighted(self.light)
        if self.level.check_goal(self.cells, self.light):
            self.current_level += 1
            # TOOD: refactor this to avoid duplicated code from the constructor
            self.level = Level.create(self.current_level)
            self.cells = self.level.get_cells()
            self.edges = []
            self.light = self.level.get_light()
            self.update_polygons()
            self.light.calculate_triangle_points(self.get_obstacles_corners(), self.edges + World.border_edges)

    def events(self, event):
        if event.type == MOUSEBUTTONUP:
            self.clicked = True
            # TODO: only for debugging
            for cell in self.cells.values():
                cell.clicked(pygame.mouse.get_pos())

        elif event.type == KEYUP:
            if event.key == K_e:
                self.editor_mode = True
            elif event.key == K_s:
                self.editor_save = self.editor_mode
            elif event.key == K_t:
                self.editor_type = True
        else:
            self.light.events(event)

    def draw(self, screen):
        for _, cell in self.cells.items():
            if cell.obstacle:
                cell.draw(screen, self.tile_map)
        for edge in self.edges:
            edge.draw(screen)
        self.light.draw_cone(screen)
        for _, cell in self.cells.items():
            if not cell.obstacle:
                cell.draw(screen, self.tile_map)
        self.light.draw_tile(screen, self.tile_map)

    def get_obstacles_corners(self):
        """ get a set of unique corners in the edges """
        corners = World.border_corners
        for edge in self.edges:
            if edge.cell.obstacle:
                corners.append(Corner(edge.x, edge.y, edge.cell))
                corners.append(Corner(*edge.get_end(), edge.cell))
        return list(set(corners))

    def switch_cell(self, i, j):
        """ use a cell as obstacle (or not) """
        if (i, j) in self.cells:
            del self.cells[(i, j)]
        else:
            cell = Cell(i, j)
            # TODO: how to get a meaningful type?
            cell.tile = 0
            self.cells[(i, j)] = cell
        self.update_polygons()
        self.light.calculate_triangle_points(self.get_obstacles_corners(), self.edges + World.border_edges)

    def get_neighbors(self, i, j):
        neighbors = {'N': None, 'E': None, 'S': None, 'W': None}

        if j > 0 and (i, j - 1) in self.cells:
            neighbor = self.cells[(i, j - 1)]
            if neighbor.tile == self.cells[(i, j)].tile:
                neighbors['N'] = neighbor
        if i < GRIDWIDTH - 1 and (i + 1, j) in self.cells:
            neighbor = self.cells[(i + 1, j)]
            if neighbor.tile == self.cells[(i, j)].tile:
                neighbors['E'] = neighbor
        if j < GRIDHEIGHT - 1 and (i, j + 1) in self.cells:
            neighbor = self.cells[(i, j + 1)]
            if neighbor.tile == self.cells[(i, j)].tile:
                neighbors['S'] = neighbor
        if i > 0 and (i - 1, j) in self.cells:
            neighbor = self.cells[(i - 1, j)]
            if neighbor.tile == self.cells[(i, j)].tile:
                neighbors['W'] = neighbor

        return neighbors

    def extend_or_init_edge(self, cell, side, neighbor):
        if neighbor is not None and side in neighbor.edges and cell.tile == neighbor.edges[side].cell.tile:
            cell.edges[side] = neighbor.edges[side]
            cell.edges[side].grow()
        else:
            edge = Edge(cell.i, cell.j, side, cell)
            self.edges.append(edge)
            cell.edges[side] = edge

    def update_polygons(self):
        """ find the polygons formed by neighboring cells """
        self.edges = []
        for _, cell in self.cells.items():
            cell.edges = {}
        for coord, cell in self.cells.items():
            if cell.tile is None:
                continue
            neighbors = self.get_neighbors(*coord)
            if neighbors['N'] is None:
                self.extend_or_init_edge(cell, 'N', neighbors['W'])
            if neighbors['E'] is None:
                self.extend_or_init_edge(cell, 'E', neighbors['N'])
            if neighbors['S'] is None:
                self.extend_or_init_edge(cell, 'S', neighbors['W'])
            if neighbors['W'] is None:
                self.extend_or_init_edge(cell, 'W', neighbors['N'])

class Game:
    def __init__(self):
        self.playing = True
        pygame.init()
        self.screen = pygame.display.set_mode((WIDTH, HEIGHT))
        pygame.display.set_caption(TITLE)

        self.clock = pygame.time.Clock()
        self.background = pygame.Surface(self.screen.get_size())
        self.background = self.background.convert()
        self.background.fill(BGCOLOR)

        self.level = Level(0)

        self.world = World()

        self.clock.tick(FPS)

    def update(self):
        dt = self.clock.tick(FPS) / 1000
        self.world.update(dt)

    def draw(self):
        self.screen.blit(self.background, (0, 0))
        if WIREFRAME:
            self.draw_grid()
        self.world.draw(self.screen)
        pygame.display.flip()

    def events(self):
        for event in pygame.event.get():
            if event.type == QUIT:
                self.playing = False
            elif event.type == KEYDOWN and event.key == K_ESCAPE:
                pygame.quit()
                self.playing = False
            else:
                self.world.events(event)

    def draw_grid(self):
        for x in range(0, WIDTH, TILESIZE):
            pygame.draw.line(self.screen, LIGHTGREY, (x, 0), (x, HEIGHT))
        for y in range(0, HEIGHT, TILESIZE):
            pygame.draw.line(self.screen, LIGHTGREY, (0, y), (WIDTH, y))


def main():
    game = Game()

    while game.playing:
        game.update()
        game.draw()
        game.events()

if __name__ == '__main__':
    main()
