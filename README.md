# Famicase 2020: Helios

![](assets/helios.jpg)

The Helios game from http://famicase.com/20/index.html

## Ideas

- lighting up dark parts
- darkening too sunny parts
- mixing colors to get a specific color light

## Project description

Idea and cover by Sean Suchara｜Designer / Illustrator｜USA

Helios is a puzzle game about the relationship of light & shadow. Control the environment around you to help direct rays of light through obstacles, to wake up the flowers, illuminate stained glass windows, dance shadows across walls, find the gaps in tree branches and many more pathways to help those lost to find their way out of the dark.

## First prototype: render light and shadows

![](assets/helios-0.1.gif)

## Tasks

- [ ] Mirroring lights?
- [ ] Soften the light (k)
- [ ] reduce the length of the light. (k)
- [ ] reduce the angle of the light. (k)
- [ ] Collect ideas for mazes
- [ ] Fix the lightning of blocks not touched on the corners

## Using Godot?

### Light and torch

- [Godot Isometric Tilemap With Light](https://www.youtube.com/watch?v=KzibhXeOa68): realistic light from a torch
- [How to Make 2D Shadows and Light Masks -Now You Know Too](https://www.youtube.com/watch?v=W4O_vNBlH6w): tiles based lighting
- [LIGHT on TILE MAPS](https://www.youtube.com/watch?v=QsCfx-jjS0U): a jump and run with light around the player.
- [Focus on shaddow](https://www.youtube.com/watch?v=ImLBM-NgnJY)
- [1:1 how to make a basic flash light from a torch](https://www.youtube.com/watch?v=hefnwhE-ZZA)
- [theory about implementation of illumination](https://www.youtube.com/watch?v=5gMaushKj6k)
- [Create Awesome 2D Lighting in Godot](https://www.youtube.com/watch?v=7HjdJ9k-jhk)

## Using pygame?

- [Pygame light and shadow casting](https://www.youtube.com/watch?v=L-2onbJfCrc)  
  [Original code in C++](https://www.youtube.com/watch?v=fc3nnG2CG8U)
- [Light Effect PyGame](https://www.youtube.com/watch?v=gPioGZSZeh0): a good example, how to calculate shadow areas (no code)
- [Light from a source and shaddow behind obstacles](https://www.youtube.com/watch?v=L-2onbJfCrc)
- [Tile-based game Part 23: Lighting Effect](https://www.youtube.com/watch?v=IWm5hi5Yrvk): code for a top down light torch

Pygame and tiles:

- [Tile-based game Part 1: Setting up](https://www.youtube.com/watch?v=3UxnelT9aCo)§<https://github.com/kidscancode/pygame_tutorials/>

## Rescued bits

```
        # if self.obstacle:
        #     for p0, p1 in light.get_next_triangle_basis():
        #         if p0[1] == p1[1]: # horizontal
        #             if light.y <= y0:
        #                 y = y0
        #             else:
        #                 y = y1
        #             if p0[1] != y:
        #                 continue
        #             left = min(p0[0], p1[0])
        #             right = max(p0[0], p1[0])
        #             if x0 >= left and x0 <= right or x1 >= left and x1 <= right:
        #                 self.lighted = True
        #                 return
        #         elif p0[0] == p1[0]: # vertical
        #             if light.x <= x0:
        #                 x = x0
        #             else:
        #                 x = x1
        #             if p0[0] != x:
        #                 continue
        #             top = min(p0[1], p1[1])
        #             bottom = max(p0[1], p1[1])
        #             if y0 >= top and y0 <= bottom or y1 >= top and y1 <= bottom:
        #                 self.lighted = True
        #                 return
        # else:
        #     for p in light.triangle_points:
        #         s0 = ((light.x, light.y), p[1:])
        #         x = x0 if light.x < p[0] else x1
        #         s1 = ((x, y0), (x, y1))
        #         if Cell.segments_intersect(s1, s0):
        #             self.lighted = True
        #             return
        #         y = y0 if light.y < p[1] else y1
        #         s1 = ((x0, y), (x1, y))
        #         if Cell.segments_intersect(s1, s0):
        #             self.lighted = True
        #             return

        # for p in light.triangle_points:
        #     s0 = ((light.x, light.y), p[1:])
        #     x = x0 if light.x < p[0] else x1
        #     s1 = ((x, y0), (x, y1))
        #     if Cell.segments_intersect(s1, s0):
        #         self.lighted = True
        #         return
        #     y = y0 if light.y < p[1] else y1
        #     s1 = ((x0, y), (x1, y))
        #     if Cell.segments_intersect(s1, s0):
        #         self.lighted = True
        #         return
```

```
        # for p0, p1 in light.get_next_triangle_basis():
        #     s0 = ((light.x, light.y), p0)
        #     x = x0 if light.x <= p0[0] else x1
        #     s1 = ((x, y0), (x, y1))
        #     # if Cell.segments_intersect(s1, s0):
        #     #     self.lighted = True
        #     #     return
        #     y = y0 if light.y <= p0[1] else y1
        #     s1 = ((x0, y), (x1, y))
        #     if 'S' in self.edges and self.i == self.edges['N'].i:
        #         if Cell.segments_intersect(s1, s0):
        #             self.lighted = True
        #             return
        #     # if p0[1] == p1[1] and x0 > p0[0] and x1 <p1[0]:
        #     if p0[1] == p1[1]:
        #         pass
        #         # print(x0, p0[0], x1, p1[0])
        #         # self.lighted = True
        #         # return

        # for p0, p1 in light.get_next_triangle_basis():
        #     triangle = ((light.x, light.y), p0, p1)
```

```
    @staticmethod
    def faster_pt_in(pt, poly):
        """ https://gamedev.stackexchange.com/questions/71526/speeding-up-point-in-polygon-for-python """
        testX, testY = pt
        last = poly[-1]
        answer = False
        for now in poly:
            nowX, nowY = now
            lastX, lastY = last
            if (nowY > testY) != (lastY > testY) and (testX < (lastX - nowX) * (testY - nowY)/(lastY - nowY) + nowX):
                answer = not answer
            last = now
        return answer
```
